import click


@click.group()
def convert():
    pass


@click.command()
@click.argument("xml_file_path", type=click.Path(exists=True, dir_okay=False))
@click.argument("out_file_path", type=click.Path(dir_okay=False))
def xml_2_gsd(xml_file_path, out_file_path):
    import hoomd
    import hoomd.deprecated

    hoomd.context.initialize()
    hoomd.deprecated.init.read_xml(filename=xml_file_path)
    hoomd.dump.gsd(filename=out_file_path, period=None, group=hoomd.group.all())


convert.add_command(xml_2_gsd)


def rename_types(in_gsd, out_gsd):
    import gsd.hoomd
    import numpy as np
    import copy

    with gsd.hoomd.open(name=in_gsd, mode="rb") as gsd_file:
        snap = gsd_file[0]
    N = snap.particles.N
    bond_count = np.zeros(N, dtype=np.int)
    for idx in snap.bonds.group.flatten():
        bond_count[idx] += 1

    new_snap = gsd.hoomd.Snapshot()
    new_snap = copy.deepcopy(snap)
    max_bonds = max(bond_count)
    bond_order_numbers = [str(_) for _ in range(max_bonds + 1)]
    new_atom_types = []

    for type_name in snap.particles.types:
        for bond_order in bond_order_numbers:
            new_atom_types.append(type_name + "_" + bond_order)

    new_snap.particles.types += new_atom_types

    for idx in range(N):

        old_type = snap.particles.types[snap.particles.typeid[idx]]
        new_type = old_type + "_" + str(bond_count[idx])
        new_type_id = new_snap.particles.types.index(new_type)
        new_snap.particles.typeid[idx] = new_type_id

    with gsd.hoomd.open(name=out_gsd, mode="wb") as gsd_file:

        gsd_file.append(new_snap)


if __name__ == "__main__":
    convert()
